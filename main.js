var Hapi = require('hapi'),
    Joi = require('joi');
    q = require('q'),
    _ = require('underscore'),
    internals = {};

internals.get = function (request, reply) {
    reply('Success!\n');
};

var http = require("http"),
    port = process.env.PORT || 1881;

var conf = require('./optin.conf'),
    Firebase = require('firebase'),
    emailClient  = require('sendgrid')(conf.sendgrid.api_key),
    cloudinary = require('cloudinary'),
    moment = require('moment-timezone'),
    express = require('express'),
    request = require('request'),
    bodyParser = require('body-parser'),
    MP = require ("mercadopago");

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// CORS
app.use(function(req, res, next) {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.set('Access-Control-Allow-Headers', 'Origin, Product-Session, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Referer, User-Agent');

  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
});

var server = app.listen(port, function () {
  var host = server.address().address
  var port = server.address().port
  console.log('Magic at http://%s:%s', host, port)
});
server.listen(port);
console.log("Server Running on "+port+".\nLaunch http://localhost:"+port);

app.get('/', function (req, res) {
    res.send("Hello!");
});

cloudinary.config(conf.cloudinary);

moment.locale('es', {
        months : [
        "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiempre", "Octubre", "Noviembre", "Diciembre"
    ],
     monthsShort : [
     "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
        "Ago", "Sep", "Oct", "Nov", "Dic"
    ],
    weekdays : [
        "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"
    ],
    weekdaysShort : [
        "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"
    ],
    weekdaysMin : [
        "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá", "Do"
    ],
    longDateFormat : {
        LT : "HH:mm A",
        LTS : "HH:mm:ss A",
        L : "DD/MM/YYYY",
        LL : "D [de] MMMM[,] YYYY",
        LLL : "dddd D [de] MMMM[,] YYYY",
        LLLL : "dddd D [de] MMMM[,] YYYY LT"
    },
    calendar : {
        lastDay : '[Ayer a las] LT',
        sameDay : '[Hoy a las] LT',
        nextDay : '[Mañana a las] LT',
        lastWeek : '[El pasado] dddd [a las] LT',
        nextWeek : 'dddd [a las] LT',
        sameElse : 'L'
    },
    relativeTime : {
        future : "en %s",
        past : "hace %s",
        s : "unos pocos segundos",
        m : "un minuto",
        mm : "%d minutos",
        h : "une hora",
        hh : "%d horas",
        d : "un día",
        dd : "%d días",
        M : "un mes",
        MM : "%d meses",
        y : "un año",
        yy : "%d años"
    }
})

function getDate(){
    var timeZone = moment.tz(moment().format(), "America/Argentina/Buenos_Aires");

    var date = {
        fullDateTime: timeZone.format('LLLL'),
        fullDate: timeZone.format('LLL'),
        calendar: timeZone.format('L'),
        time: timeZone.format('LT'),
        fullTime: timeZone.format('LTS'),
        year: timeZone.get('year'),
        month: timeZone.get('month'),
        week: timeZone.get('week'),
        day: timeZone.get('date'),
        hour: timeZone.get('hour'),
        minutes: timeZone.get('minutes'),
         dateInMilliseconds: moment(timeZone).valueOf(),
    };

    return date
}

//Log catcher setup
var firebaseRef = new Firebase(conf.firebase.url);

//Firebase Auth
firebaseRef.authWithCustomToken(conf.firebase.secret, function(error) {
    //Auth Failed
    if (error) {
        console.log(error);
    } else {
        console.log('Firebase: Listening');
        // New Optin Queued
        firebaseRef.child(conf.firebase.queue).on('child_added', function(optinSnapshot) {
            var optinData = optinSnapshot.val();
            var optinLocation = conf.firebase.url + conf.firebase.queue + '/' + optinSnapshot.key();
            var optinRef = new Firebase(optinLocation);
            var emailDomain = '';
            if(optinData.email) {
                emailDomain = optinData.email.slice(optinData.email.lastIndexOf('@') + 1);
            }
            else {
                emailDomain = 'forbidden.com';
            }

            var newPassword = Math.random().toString(36).slice(-8);
            firebaseRef.createUser({
                email: optinData.email,
                password: newPassword
            }, function(error, userData) {
                if (error) {
                    optinRef.remove(function(error) {
                        if (error) {
                            console.log(error);
                        }
                    });
                } else {
                    firebaseRef.child('users').child(userData.uid).set({
                        email: optinData.email,
                        role: 'customer',
                        registered: false
                    }, function(error) {
                        if(!error) {
                            var email = new emailClient.Email({
                                            to:       optinData.email,
                                            from:     'info.ceiboit@gmail.com',
                                            subject:  'Bienvenido a Las Coloradas!',
                                            html: '<h2>Contraseña temporal: </h2>'+  newPassword, // This fills out the <%body%> tag inside SendGrid template
                                        });

                            email.setFilters({"templates": {"settings": {"enabled": 1, "template_id": conf.sendgrid.register_template}}});
                            //send mail
                            emailClient.send(email, function(error, json) {
                                !error ? optinRef.remove(function(error) {}) : console.log(error);
                            });
                        }

                    });

                }
            });

            //Auth Lost
        });

        // New Sales Queued
        firebaseRef.child('jobs').child(conf.firebase.sales).on('child_added', function(saleSnapshot) {
            var saleData = saleSnapshot.val();
            var saleLocation = conf.firebase.url + 'jobs/' + conf.firebase.sales + '/' + saleSnapshot.key();
            var saleRef = new Firebase(saleLocation);

            var salePendingLocation = conf.firebase.url + conf.firebase.sales + '/pending'
            var salePendingRef = new Firebase(salePendingLocation);

            var items = saleData[0];
            var userData = saleData[1];
            var saleDate = getDate();
            var templateProducts = '';
            var total = 0;

            for (var i = 0; i < items.length; i++) {
                items[i].name = items[i].title
                total +=  items[i].price * items[i].quantity;

                //template products
                templateProducts += '<b>Nombre: </b>' + items[i].name + '<br >' +
                                    '<b>Cantidad: </b>' + items[i].quantity + '<br >' +
                                    '<b>Precio: </b>' +'$'+ items[i].price + '<br >' +
                                    '<b>Subtotal: </b>' +'$'+ items[i].price * items[i].quantity + '<br >' +
                                    '-' + '<br >';
            };
 
            saleRef.remove(function(error) {
                if (error) {
                    console.log(error)
                }else{
                    salePendingRef.push({ date: saleDate, 'items': items, 'user': userData.id, 'status': 'pending' })
                }
            });


            //For user
            var email = new emailClient.Email({
                            to:       userData.email,
                            from:     'info.ceiboit@gmail.com',
                            subject:  'Gracias por su compra!',
                            html:      '<h2>Detalle de la compra:</h2>' +
                                        '<b>Fecha de la compra: </b>' + saleDate.fullDateTime + '<br >' +
                                        '<h3>Productos: </h3>' + templateProducts + '<br >' +
                                        '<b>Total: </b>' +'$'+ total + '<br >' +
                                        '----' + '<br >' +
                                       'En breve será contactado por uno de nuestros representantes.',
                        });

            email.setFilters({"templates": {"settings": {"enabled": 1, "template_id": conf.sendgrid.checkout_client_template}}});
            //send mail
            emailClient.send(email, function(error, json) {
                !error ? saleRef.remove(function(error) {}) : console.log(error);
            });

            //For admin
            var email = new emailClient.Email({
                            to:       'armerialascoloradas@gmail.com',
                            from:     'info.ceiboit@gmail.com',
                            subject:  'Nueva venta en el sitio!',
                            html:      '<b>Fecha de la venta: </b>' + saleDate.fullDateTime + '<br >' +
                                       '----' +
                                       '<h3>Datos del usuario: </h3>' +
                                       '<b>id: </b>' + userData.id + '<br >' +
                                       '<b>Email: </b>' + userData.email + '<br >' +
                                       '<b>Nombre y apellido: </b>' + userData.firstName + ' ' + userData.lastName + '<br >' +
                                       '<b>DNI: </b>' + userData.dni + '<br >' +
                                       '<b>CUIL: </b>' + userData.cuil + '<br >' +
                                       '<b>Teléfono: </b>' + userData.phone + '<br >' +
                                       '<b>Ciudad: </b>' + userData.city + '<br >' +
                                       '<b>Dirección: </b>' + userData.street + '<br >' +
                                       '<b>Código postal: </b>' + userData.zip + '<br >' +
                                       '----' +
                                       '<h3>Detalle de la venta: </h3>' +
                                       templateProducts +
                                       '<br>' +
                                       '<b>Total: </b>' +'$'+ total + '<br >',
                        });

            email.setFilters({"templates": {"settings": {"enabled": 1, "template_id": conf.sendgrid.checkout_admin_template}}});
            //send mail
            emailClient.send(email, function(error, json) {
                !error ? saleRef.remove(function(error) {}) : console.log(error);
            });
        });

        // New contact Queued
        firebaseRef.child('jobs').child(conf.firebase.contact).on('child_added', function(contactSnapshot) {
            var contactData = contactSnapshot.val();
            var contactLocation = conf.firebase.url + 'jobs/' + conf.firebase.contact + '/' + contactSnapshot.key();
            var contactRef = new Firebase(contactLocation);
            
            if(!contactData.phone) contactData.phone = 'No definido.';

            var email = new emailClient.Email({
                            to:       'armerialascoloradas@gmail.com',
                            from:     'info.ceiboit@gmail.com',
                            subject:  'Nuevo mensaje recibido!',
                            html: '<h3>Email: </h3>'+  contactData.email + 
                                  '<h3>Nombre: </h3>'+ contactData.name +
                                  '<h3>Teléfono: </h3>'+ contactData.phone + 
                                  '<h3>Mensaje: </h3>'+ contactData.message,
                        });

            email.setFilters({"templates": {"settings": {"enabled": 1, "template_id": conf.sendgrid.contact_template}}});
            //send mail
            emailClient.send(email, function(error, json) {
                !error ? contactRef.remove(function(error) {}) : console.log(error);
            });

        });

        // New cleanUp Queued
        firebaseRef.child('jobs').child(conf.firebase.cleanUp).on('child_added', function(cleanUpSnapshot) {
            var cleanUpId = cleanUpSnapshot.val();
            var cleanUpLocation = conf.firebase.url + 'jobs/' + conf.firebase.cleanUp + '/' + cleanUpSnapshot.key();
            cloudinary.uploader.destroy(cleanUpId, function(res) {
                if (res.result === 'ok') {
                    var cleanUpref = new Firebase(cleanUpLocation);
                    cleanUpref.remove(function(error) {
                        if (error) {
                           console.log(error);
                        }
                    });
                }
            });
        });

    }
});

var CLIENT_ID = '';
var CLIENT_SECRET = '';
var mp;
var mpConfigGet = function() {
    var prom = q.defer();
    firebaseRef.child('settings').on('value', function(data) {
        var data = data.val().mpCredential;
        var key = _.keys(data)[0];
        if (data && key != 'undefined') {
            CLIENT_ID = data[key].identif;
            CLIENT_SECRET = data[key].secret;
        };

       // Mercadopago: configuration & auth
        mp = new MP (CLIENT_ID, CLIENT_SECRET);
        mp.getAccessToken(function (err, accessToken){
            if (err) {
                console.log(err);
                prom.resolve();
            } else {
                console.log ("Mercadopago Token: Received");
                prom.resolve(accessToken);
            }
        });
    });

    return prom.promise;
};


// Mercadopago: ver pagos y su estado
app.get('/payments', function (req, res) {
    return mpConfigGet().then(function() {
        var filters = {};
        mp.searchPayment(filters, offset=0, limit=0)
            .then (
                function success (data) {
                    res.send(JSON.stringify(data, null, 4));
                },
                function error (err) {
                    res.send(err);
                }
            );
    });
});

// Mercadopago: ver pagos y su estado
app.get('/payments/:saleId', function (req, res) {
    return mpConfigGet().then(function() {
        var filters = {
            "id": req.params.saleId
        };

        mp.searchPayment (filters)
            .then (
                function success (data) {
                    res.send(JSON.stringify(data, null, 4));
                },
                function error (err) {
                    res.send(err);
                }
            );
    });
});

// Mercadopago: creación de pago
app.post('/preferences', function (req, res) {
    return mpConfigGet().then(function() {
        var preference = req.body;
        mp.createPreference (preference, function (err, data){
            if (err) {
                res.send(err);
            } else {
                // url para terminar el pago
                res.send(data.response.init_point);

                // debería abrirse una ventana de este estilo
                //window.open(data.response.init_point, 'Mercadopago', "width=800, height=600");
            }
        });
    });
});

// Mercadopago: ver pagos y su estado
app.get('/preferences/:preferenceId', function (req, res) {
    return mpConfigGet().then(function() {
        mp.getPreference (req.params.preferenceId)
            .then (
                function success (data) {
                    res.send(JSON.stringify(data, null, 4));
                },
                function error (err) {
                    res.send(err);
                }
            );
    });
});