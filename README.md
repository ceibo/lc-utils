Módulo complementario para la aplicación de Las Coloradas. Provee funcionalidades que
no están directamente relacionadas con la lógica de negocio de un ecommerce, sino mas 
bien, de intereacción con otros servicios 3rd-party.
Se encarga de:
     
Alta de nuevos usuarios. La autenticacion de Las Coloradas es manejada a través 
de firebase. Cuando un usario nuevo se registra, este módulo se encarga de darlo de
alta en firebase y de enviarle el mail.
     
Ventas. Cuando una venta se realiza, se recolecta la info de la venta en el client,
se arma un json con esa info y se lo envía a este módulo.
Este módulo se encarga de registrar esa venta en firebase.
     
Envío de mails a través de mandrill. Cuando un usuario nuevo se registra,
se le envía un correo de bienvenida y se le notifica la password. 
Ese mail sale a través de mandril.